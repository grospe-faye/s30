const express = require('express');
const mongoose = require('mongoose');
const port = 4000;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));



// Syntax:
/*
	mongoose.connect('<connection string> {middleware}')
*/

mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.2ci5m.mongodb.net/session30?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
	
	// console.error.bind(console) - print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "Connection Error"))

	// if the connection is successful, this will be the output in our console
	db.once('open', () => console.log('Connected to the cloud database'))

// Mongoose Schema
const taskSchema = new mongoose.Schema({

	// define the name of our schema - taskSchema
	// new mongoose.Schema method - to make
	// we will be needing the name of the task and its status
	// each field will require a data type

	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});

const Task = mongoose.model('Task', taskSchema);
	// models use schemas and they act as the middleman from the server to our database
	// model can now be sed to run commands for interacting with our database
	// name of model should be capitalized and singular form - 'Task'
	// second parameter is used to specify the schema of the documents that will be stored in the mongoDB collection

// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
	-if the task already exists in th db, we return an error
	-if the task doesn't exists in the db, we add it in the dn=b.
	2. The task data will be coming from the request body
	3. Create a new Task object with name field property
*/

app.post('/tasks', (req, res) => {
	// check any duplicate task
	// err shorthand of error
	Task.findOne({name: req.body.name}, (err, result) =>{

		// if there are matches,
		if(result != null && result.name === req.body.name){

			// will return this message
			return res.send('Duplicate task found.')
		} else{

			let newTask = new Task({
				name: req.body.name
			})

			// save method will accept a callback function which stores any errors in the first parameter and will store the newly saved document in the second parameter
			newTask.save((saveErr, savedTask) => {

				// if there are many errors, it will print the error
				if(saveErr){
					return console.error(saveErr)

				// no error found, return the status code and the message
				} else{
					return res.status(201).send('New Task Created')
				}
			})
		}
	})
})

// Retrieving all tasks
app.get('/tasks', (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

/*ACTIVITY*/
const userSchema = new mongoose.Schema({
	name: String,
	password: String
});
const User = mongoose.model('User', userSchema);

// create a post route to register 3 users
app.post('/signup', (req, res) => {

	User.findOne({name: req.body.name}, (err, result) =>{

		if(result != null && result.name === req.body.name){
			return res.send('User already exists.')
		} else{

			let newUser = new User({
				name: req.body.name
			})

			newUser.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				} else{
					return res.status(201).send('New User Created')
				}
			})
		}
	})
})

// Retrieve all users
app.get('/users', (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at ${port}`))